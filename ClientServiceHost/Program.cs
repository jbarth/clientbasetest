﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel.Description;
using ClientService;

namespace ClientServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost testServiceHost = null;
            try
            {
                //Base Address for StudentService
                const string UriString = "http://127.0.0.1:80/wcf";
                Uri httpBaseAddress = new Uri(UriString);
                //Instantiate ServiceHost
                testServiceHost = new ServiceHost(typeof(ClientService.ClientService),
httpBaseAddress);
                //Add Endpoint to Host
                testServiceHost.AddServiceEndpoint(typeof(ClientService.IClientService),
                                                        new WSHttpBinding(), "");
                //Metadata Exchange
                ServiceMetadataBehavior serviceBehavior = new ServiceMetadataBehavior();
                serviceBehavior.HttpGetEnabled = true;
                testServiceHost.Description.Behaviors.Add(serviceBehavior);

                //Open
                testServiceHost.Open();
                Console.WriteLine("Service is live now at: "+httpBaseAddress.ToString());
                Console.WriteLine("Press enter to end service.");
                Console.WriteLine("---------------------------");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                testServiceHost = null;
                Console.WriteLine("There is an issue with ClientService: ["+ex.Source+"]: "+ex.Message+"|"+ex.StackTrace);
                Console.ReadKey();
            }
        }
    }
}
