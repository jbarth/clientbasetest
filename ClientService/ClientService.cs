﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ClientService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ClientService" in both code and config file together.
    public class ClientService : IClientService
    {
        public string SendMsg(string msg)
        {
            Console.WriteLine("Message Received: "+msg);
            return "Successfully received message \""+msg+"\"";
        }
    }
}
