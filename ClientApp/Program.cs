﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientService;

namespace ClientApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ensure the Service is running before sending a message.");
            Console.WriteLine("-------------------------------------------------------");
            ClientAppProxy myclient;
            try
            {
                myclient = new ClientAppProxy();
                while (true) {
                    Console.Write("Message to Send or \"exit\" to quit:");
                    string msg = Console.ReadLine();
                    if (msg.ToLower() == "exit")
                    {
                        return;
                    }
                    string res = myclient.SendMsg(msg);
                    Console.WriteLine("Response: "+res);
                    Console.WriteLine("-------------------");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e.Message);
                Console.ReadKey();
            }
        }
    }
}
