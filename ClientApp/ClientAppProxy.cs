﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using ClientService;

namespace ClientApp
{
    public class ClientAppProxy: ClientBase<IClientService>, IClientService
    {
        public string SendMsg(string msg)
        {
            return base.Channel.SendMsg(msg);
        }
    }
}
